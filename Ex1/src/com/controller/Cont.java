package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Cont {
	@RequestMapping("/home.htm")
	public ModelAndView home(){
		ModelAndView modelAndView=new ModelAndView("home");
		modelAndView.addObject("msg", "Hello World");
		System.out.println("Hello World");
		return modelAndView;
	}

}
